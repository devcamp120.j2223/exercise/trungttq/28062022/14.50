import { Component } from "react";

class FetchAPI extends Component {

    fetchAPI = async (url, body) => {
        let response = await fetch(url, body);
        let data = await response.json();

        return data;
    }

    getAllAPI = () => {
        this.fetchAPI("http://42.115.221.44:8080/devcamp-pizza365/orders")
            .then((data) => {
                console.log(data);
            })
    }

    getByIDAPI = () => {
        let vOrderId = "z9gJp0K07w";
        this.fetchAPI("http://42.115.221.44:8080/devcamp-pizza365/orders" + "/" + vOrderId)
            .then((data) => {
                console.log(data);
            })
    }

    postAPI = () => {
        let body = {
            method: 'POST',
            body: JSON.stringify({
                kichCo: "M",
                duongKinh: "25",
                suon: "4",
                salad: "300",
                loaiPizza: "HAWAII",
                idVourcher: "16512",
                idLoaiNuocUong: "PEPSI",
                soLuongNuoc: "3",
                hoTen: "sdsdsds",
                thanhTien: "200000",
                email: "sdsds@",
                soDienThoai: "454554",
                diaChi: "Hà Nội",
                loiNhan: "Pizza đế dày"
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        };

        this.fetchAPI("http://42.115.221.44:8080/devcamp-pizza365/orders", body)
            .then((data) => {
                console.log(data);
            })
    }

    updateByIDAPI = () => {
        let vOderId = "438494";
        let body = {
            method: 'PUT',
            body: JSON.stringify({
                trangThai: "confirmed"
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        };

        this.fetchAPI("http://42.115.221.44:8080/devcamp-pizza365/orders" + "/" + vOderId, body)
            .then((data) => {
                console.log(data);
            })
    }

    getAllDrinkAPI = () => {
        this.fetchAPI("http://42.115.221.44:8080/devcamp-pizza365/drinks")
            .then((data) => {
                console.log(data);
            })
    }

    render() {
        return (
            <div className="row mt-2">
                <div className="col-2">
                    <button className="btn btn-info" onClick={this.getAllAPI}>Call API get all order</button>
                </div>
                <div className="col-2">
                    <button className="btn btn-info" onClick={this.getByIDAPI} >Call API get order by ID</button>
                </div>
                <div className="col-2">
                    <button className="btn btn-info" onClick={this.postAPI}>Call API create order</button>
                </div>
                <div className="col-2">
                    <button className="btn btn-info" onClick={this.updateByIDAPI}>Call API Update Order</button>
                </div>
                <div className="col-2">
                    <button className="btn btn-info" onClick={this.getAllDrinkAPI}>Call API get drink list</button>
                </div>
            </div>
        )
    }
}

export default FetchAPI;