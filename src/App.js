import "bootstrap/dist/css/bootstrap.min.css";
import AxiosLibrary from "./components/AxiosLibrary";

import FetchAPI from "./components/FetchAPI";

function App() {
  return (
    <div className="container">
      <FetchAPI />
      <AxiosLibrary/>
    </div>
  );
}

export default App;
